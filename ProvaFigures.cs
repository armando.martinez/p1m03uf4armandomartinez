﻿using System;

namespace P1M03UF4ArmandoMartínez
{
    class FiguraGeometrica
    {
        protected int codi { get; set; }
        protected string nom { get; set; }
        protected ConsoleColor color { get; set; }

    }

    class Rectangle : FiguraGeometrica
    {
        public double width { get; set; }
        public double height { get; set; }
        
        public Rectangle()
        {
            this.codi = 1;
            this.nom = "Rectangle";
            this.color = ConsoleColor.White;
            this.width = 1;
            this.height = 1;
        }
        public Rectangle(int codi, string nom, ConsoleColor color, double width, double height)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.width = width;
            this.height = height;
        }
        public Rectangle(Rectangle prevRectangle)
        {
            this.codi = prevRectangle.codi;
            this.nom = prevRectangle.nom;
            this.color = prevRectangle.color;
            this.width = prevRectangle.width;
            this.height = prevRectangle.height;
        }

        public string ToString()
        {
            return $"Codi: {this.codi}\nNom: {this.nom}\nColor: {this.color.ToString()}\nBase: {this.width}\nAltura: {this.height}\n";
        }
        public double Perimetre()
        {
            return (this.height * 2) + (this.width * 2);
        }
        public double Area()
        {
            return this.height * this.width;
        }
        public void Draw()
        {
            // Escribe el nombre
            Console.WriteLine(this.nom);
            for (int i = 0; i < this.nom.Length; i++) { Console.Write('-'); }

            // Dibuja el rectangulo
            Console.ForegroundColor = this.color;
            for (int i = 0; i < this.height; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < this.width; j++)
                {
                    Console.Write("* ");
                }
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
        }
    }

    class Triangle : FiguraGeometrica
    {
        public double width { get; set; }
        public double height { get; set; }
        
        public Triangle()
        {
            this.codi = 1;
            this.nom = "Triangle";
            this.color = ConsoleColor.White;
            this.width = 1;
            this.height = 1;
        }
        public Triangle(int codi, string nom, ConsoleColor color, double width, double height)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.width = width;
            this.height = height;
        }
        public Triangle(Triangle prevTriangle)
        {
            this.codi = prevTriangle.codi;
            this.nom = prevTriangle.nom;
            this.color = prevTriangle.color;
            this.width = prevTriangle.width;
            this.height = prevTriangle.height;
        }

        public string ToString()
        {
            return $"Codi: {this.codi}\nNom: {this.nom}\nColor: {this.color.ToString()}\nBase: {this.width}\nAltura: {this.height}\n";
        }
        public double Perimetre()
        {
            return this.height + this.width + Math.Sqrt(Math.Pow(this.height, 2) + Math.Pow(this.width, 2));
        }
        public double Area()
        {
            return this.height * this.width / 2;
        }
        public void Draw()
        {
            // Escribe el nombre
            Console.WriteLine(this.nom);
            for (int i = 0; i < this.nom.Length; i++) { Console.Write('-'); }
            Console.WriteLine();

            // Dibuja el triangulo
            Console.ForegroundColor = this.color;
            for (int i = 0; i < this.height; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < this.width - i; j++)
                {
                    Console.Write("* ");
                }
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }

    class Cercle : FiguraGeometrica
    {
        public double radi { get; set; }

        public Cercle()
        {
            this.codi = 1;
            this.nom = "Cercle";
            this.color = ConsoleColor.White;
            this.radi = 1;
        }
        public Cercle(int codi, string nom, ConsoleColor color, double radi)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.radi = radi;
        }
        public Cercle(Cercle prevCercle)
        {
            this.codi = prevCercle.codi;
            this.nom = prevCercle.nom;
            this.color = prevCercle.color;
            this.radi = prevCercle.radi;
        }
        
        public string ToString()
        {
            return $"Codi: {this.codi}\nNom: {this.nom}\nColor: {this.color.ToString()}\nRadi: {this.radi}\n";
        }
        public double Perimetre()
        {
            return 2 * Math.PI * this.radi;
        }
        public double Area()
        {
            return Math.PI * Math.Pow(this.radi, 2);
        }
        public void Draw()
        {
            // Escribe el nombre
            Console.WriteLine(this.nom);
            for (int i = 0; i < this.nom.Length; i++) { Console.Write('-'); }
            Console.WriteLine();

            // Dibuja el circulo
            Console.ForegroundColor = this.color;
            for (double y = -this.radi; y <= this.radi; y++)
            {
                for (double x = -this.radi; x <= this.radi; x++)
                {
                    if (x * x + y * y <= this.radi * this.radi)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }


    class ProvaFigures
    {
        static void Main()
        {
            Cercle example = new Cercle(1, "GRAN ESFERA DE TURBO MUERTE", ConsoleColor.Yellow, 6);

            example.Draw();
            Console.WriteLine(example.ToString());
            Console.WriteLine(example.Area());

            new Cercle().Draw();

            Rectangle rect = new Rectangle(2, "CUBO XD", ConsoleColor.Red, 6, 3);
            rect.Draw();
            Console.WriteLine(rect.ToString());
            Console.WriteLine();
            new Triangle(3, "HEHE", ConsoleColor.Cyan, 8, 4).Draw(); ////// CAMBIAR DRAW
        }
    }
}
